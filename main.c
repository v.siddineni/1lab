


#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <sys/time.h>
#define SEED 35791246

double Calculate_Pi_Sequential(long int number_of_tosses) {

    srand(SEED);
    long i, count;
    double x, y,z,pi;
    count = 0;
    for(i=0;i<number_of_tosses;i++){
        x=(double)rand()/RAND_MAX;
        y=(double)rand()/RAND_MAX;
        if (x*x + y*y <= 1.0) count++;
    }

    return (double)count/number_of_tosses*4;

}


long i, count;
double x, y,z,pi;
long double Calculate_Pi_Parallel(long int number_of_tosses) {


    srand(SEED);
    //omp_set_num_threads(4);

#pragma omp for reduction(+:count) private(x,y)
    for (i=0; i<number_of_tosses; i++)              //main loop
    {
        x = (double)rand()/RAND_MAX;      //gets a random x coordinate
        y = (double)rand()/RAND_MAX;      //gets a random y coordinate
        if (x*x + y*y <= 1.0) count++;
    }

    return (double)count/number_of_tosses*4;

}

int main() {

    double sequential_pi, parallel_pi;
    struct timeval start, end;
    long int niter;

    //printf("Enter the iteration:");
    //scanf("%ld",&niter);

    niter = 100000;

    gettimeofday(&start, NULL);
    printf("Timing sequentiona1...\n");
    sequential_pi = Calculate_Pi_Sequential(niter);
    gettimeofday(&end, NULL);
    printf("Took %.6f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000L);


    printf("Timing parallel...\n");
    gettimeofday(&start, NULL);
    parallel_pi = Calculate_Pi_Parallel(niter);
    gettimeofday(&end, NULL);
    printf("Took %.6f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000L);

    printf("π = %.10f (sequential)\n", sequential_pi);
    printf("π = %.10f (parallel)\n", parallel_pi);

    return 0;
    //samples  = 1000000000;

}

